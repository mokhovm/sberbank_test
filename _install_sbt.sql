-- данный скрипт устанавливает пример для Сбербанка

@install/sbt_support_db_objects_install.sql;
@_sbt_compile.sql;

begin
  sbt_utils.clear_test_data();
  sbt_utils.generate_test_data();
end;
/