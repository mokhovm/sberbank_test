-- пользователь для схемы
create user sbt
  identified by "sbt"
    default tablespace users quota unlimited on users;

grant connect, create any context, create job, create public synonym, create synonym, 
create view, drop public synonym, resource, create trigger, create procedure to sbt;
