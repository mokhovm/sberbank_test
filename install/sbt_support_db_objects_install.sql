/*
  Скрипт инсталляции объектов для теста Сбербанка
  @author Мохов Михаил
*/

-- универсальная секвенция
create sequence uni_seq start with 1 increment by 1;


/*
  @desctiption хранит данные для графиков
*/
create table table_1 
(
   c_id number not null,
   c_name varchar2(255) not null,
   c_type varchar2(255) not null,
   c_date date,
   c_value number,
   constraint pk_table_1 primary key (c_id)
);

comment on table table_1 is
'Используется для хранения данных линий графика';

comment on column table_1.c_name is
'наименование линии';

comment on column table_1.c_type is
'тип линии';

comment on column table_1.c_date is
'дата';

comment on column table_1.c_value is
'значение';

/*
  @desctiption хранит данные для дерева
*/
create table table_2 
(
   c_id number not null,
   c_pid number references table_2,
   c_name varchar2(255),
   c_val number,
   c_status number,
   constraint pk_table_2 primary key (c_id)
);

comment on table table_2 is
'Используется для хранения данных для дерева';

comment on column table_2.c_id is
'идентификатор';

comment on column table_2.c_pid is
'родитель';

comment on column table_2.c_name is
'наименование';

comment on column table_2.c_val is
'значение';

comment on column table_2.c_status is
'статус';

