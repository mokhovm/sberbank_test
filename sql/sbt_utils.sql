create or replace package sbt_utils is
  /*
    Description: всякие вспомогательные утилиты для sberbank test
  */

  REC_NOT_FOUND constant number := -1;
  -- количество тестовых записей для каждой линии
  MAX_SAMPLE_DATA constant number := 10;

  -- Генерирует тестовые данные для примера
  procedure generate_test_data;
  -- Удаляет данные примера
  procedure clear_test_data;

  -- Добавляет или апдейит данные в table_1. Для добавления нужно передать в качестве p_id константу REC_NOT_FOUND
  function upsertData1(p_id in number, p_name in varchar2, p_type in varchar2, p_date in date,
    p_value in number) return number;

  -- Генерирует случайную величину для table_2
  function get_random_value return number;
  --  Генерирует случайный статус для table_2
  function get_random_status return number;


end;
/

create or replace package body sbt_utils is

  -- объявим новый тип коллекции
  type Setting_list is table of Line_settings;

  -- коллекция с настройками для генерции данных
  settings Setting_list := Setting_list(Line_settings('Sugar', 'solid', 30, 120),
    Line_settings('Buckwheat', 'solid', 10, 100), Line_settings('Matches', 'solid', 5, 50),
    Line_settings('Salt', 'dotted', 1, 50), Line_settings('Gold', 'dashed', 70, 300));

  /*
    Генерирует тестовые данные для таблицы 1
  */
  procedure generate_data_for_t1 is
    cur_date date;
    line Line_settings;
  begin
    for idx in 1..settings.count loop
      line := settings(idx);
      for cnt in 1..MAX_SAMPLE_DATA loop
        cur_date := sysdate + cnt;
        insert into table_1 (c_id, c_name, c_type, c_date, c_value)
        values (uni_seq.nextval, line.line_name, line.line_type, cur_date,
          round(dbms_random.value(line.min_value, line.max_value), 2));
      end loop;
    end loop;
  end;


  /*
    Генерирует случайную величину для table_2
   */
  function get_random_value return number is
  begin
    return round(dbms_random.value(0, 500));
  end;


  /*
    Генерирует случайный статус для table_2
   */
  function get_random_status return number is
  begin
    return round(dbms_random.value(0, 10));
  end;

  /*
    Генерирует тестовые данные для таблицы 2
  */
  procedure generate_data_for_t2 is
  begin

    -- т.к. таблица с иерархией, создадим для неё разумные данные, чтобы было проще следить корректностью
    -- построения дерева
    insert into table_2 (c_id, c_pid, c_name, c_val, c_status)
    values (1, null, 'Computer games', get_random_value, get_random_status);
    insert into table_2 (c_id, c_pid, c_name, c_val, c_status)
    values (2, 1, 'Action', get_random_value, get_random_status);
    insert into table_2 (c_id, c_pid, c_name, c_val, c_status)
    values (3, 1, 'Stategy', get_random_value, get_random_status);
    insert into table_2 (c_id, c_pid, c_name, c_val, c_status)
    values (4, 1, 'Simulator', get_random_value, get_random_status);
    insert into table_2 (c_id, c_pid, c_name, c_val, c_status)
    values (5, 2, 'Shooter', get_random_value, get_random_status);
    insert into table_2 (c_id, c_pid, c_name, c_val, c_status)
    values (6, 2, 'Fighting', get_random_value, get_random_status);
    insert into table_2 (c_id, c_pid, c_name, c_val, c_status)
    values (7, 2, 'Splasher', get_random_value, get_random_status);
    insert into table_2 (c_id, c_pid, c_name, c_val, c_status)
    values (8, 3, 'Wargame', get_random_value, get_random_status);
    insert into table_2 (c_id, c_pid, c_name, c_val, c_status)
    values (9, 3, 'Economic', get_random_value, get_random_status);
    insert into table_2 (c_id, c_pid, c_name, c_val, c_status)
    values (10, 3, 'Card', get_random_value, get_random_status);
    insert into table_2 (c_id, c_pid, c_name, c_val, c_status)
    values (11, 4, 'Sport simulator', get_random_value, get_random_status);
    insert into table_2 (c_id, c_pid, c_name, c_val, c_status)
    values (12, 4, 'Manage simulator', get_random_value, get_random_status);
    insert into table_2 (c_id, c_pid, c_name, c_val, c_status)
    values (13, 4, 'Build simulator', get_random_value, get_random_status);
    insert into table_2 (c_id, c_pid, c_name, c_val, c_status)
    values (14, 4, 'Tech simulator', get_random_value, get_random_status);
  end;

  /*
    Генерирует тестовые данные для примера
  */
  procedure generate_test_data is
  begin
    generate_data_for_t1;
    generate_data_for_t2;
    commit;
  end;

  /*
    Удаляет данные примера
  */
  procedure clear_test_data is
  begin
    execute immediate 'truncate table table_1';
    execute immediate 'truncate table table_2';
  end;

  /*
   Добавляет или апдейит данные в table_1. Для добавления нужно передать в качестве p_id константу REC_NOT_FOUND
   @param p_id - ключ записи
   @param p_name - название линии
   @param p_type - тип линии
   @param p_date - дата измерения
   @param p_value - значение
   @return - вернет ид обработанной записи
   */
  function upsertData1(p_id in number, p_name in varchar2, p_type in varchar2, p_date in date,
    p_value in number) return number is
    res number;
  begin

    if (p_id = REC_NOT_FOUND) then
      select uni_seq.nextval into res from dual;
    else
      res := p_id;
    end if;

    merge into table_1
    using dual on (c_id = p_id)
      when matched then update set c_name = p_name, c_type = p_type, c_date = p_date, c_value = p_value
      when not matched then insert(c_id, c_name, c_type, c_date, c_value)
      values(res, p_name, p_type, p_date, p_value);
    return res;
  end;


end;
/