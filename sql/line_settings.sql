create or replace type line_settings force as object
(
  /*
    Настройки параметров для линий, по которым будут строится графикк
  */

  -- название линии
  line_name varchar2(255),
  -- тип линии
  line_type varchar2(255),
  -- минимальное значение
  min_value integer,
  -- максимальное значение
  max_value integer

) final;
/


